public class MethodsTest {
    public static void main(String[] args) {
        
        int x = 10;

        System.out.println(x);
        methodNoInputNoReturn();
        System.out.println(x);

        methodOneInputNoReturn(10);
        methodOneInputNoReturn(x);
        methodOneInputNoReturn(x+50);

        methodTwoInputNoReturn(x, 1.1);

        int y = methodNoInputReturnInt();
        System.out.println(y);

        double sqrt = sumSquareRoot(6, 3);
        System.out.println(sqrt);

        String s1 = "Hello";
        String s2 = "Goodbye";

        System.out.println(s1.length());

        System.out.println(SecondClass.addOne(50));
        
        SecondClass sc = new SecondClass();
        System.out.println(sc.addTwo(50));

    }

    public static void methodNoInputNoReturn() {
        
        int x = 50;

        System.out.println(x);

    }

    public static void methodOneInputNoReturn(int x) {
        
        System.out.println("Inside the method one input no return");
        System.out.println(x);

    }

    public static void methodTwoInputNoReturn(int x, double y) {
        
    }

    public static int methodNoInputReturnInt() {
        
        return 6;

    }

    public static double sumSquareRoot(int x, int y) {
        
        int sum = x + y;

        return Math.sqrt(sum);

    }
}