import java.util.Scanner;

public class PartThree {
    public static void main(String[] arg){

        Scanner key = new Scanner(System.in);
        AreaComputations ac = new AreaComputations();

        System.out.print("Enter square length: ");
        int sLength = key.nextInt();

        System.out.print("Enter rectangle length: ");
        int rLength = key.nextInt();

        System.out.print("Enter rectangle width: ");
        int rWidth = key.nextInt();

        System.out.println(AreaComputations.areaSquare(sLength));
        System.out.println(ac.areaRectangle(rLength, rWidth));

    }
}
